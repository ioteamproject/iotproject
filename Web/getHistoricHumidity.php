<?php

  include "db_connect.php";

  $db=connectToDatabase();
  if($db->connect_error){
      die("Unable to connect database: " . $db->connect_error);
  }

  //get user data from the database
  $query = $db->query("SELECT * FROM humidity LIMIT 100");
  $i = 0;

  if($query->num_rows > 0){

    $data['status'] = 'ok';
    while ($i <= $query->num_rows) {
      $userData = $query->fetch_assoc();
      if (!$userData == null) {
        $data['result'][] = $userData;
      }
      $i++;

    }
  }else{
      $data['status'] = 'err';
      $data['result'] = '';
  }

  $db->close();
  echo json_encode($data, JSON_PRETTY_PRINT);

?>
