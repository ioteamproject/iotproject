var time = 3000;
var timestamp = [];
var statoAttuale = "STATO ATTUALE";
var state = document.getElementById("state");
var getState = function() {
  $.ajax({
    type: 'POST',
    url: 'getState.php',
    dataType: "json",
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){

          if (!timestamp.includes(data.result.timestamp)){
            timestamp.push(data.result.timestamp);
            state.innerHTML = data.result.mode;
        }
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

getState();
setInterval(getState,time);
