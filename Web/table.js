var tableRef = document.getElementById("table");

var time = 3000;
var id = 0;
var getLog = function() {
  $.ajax({
    type: 'POST',
    url: 'getLog.php',
    dataType: "json",
    data : {id:id},
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures

      if (data.status == 'ok'){
        //console.log("Dati arrivati");
        var newRow = table.insertRow(0);
        var idCell = newRow.insertCell(0);
        var logCell = newRow.insertCell(1);
        var timeStampCell = newRow.insertCell(2);
        var idText = document.createTextNode(data.result.id);
        var logText = document.createTextNode(data.result.string);
        var timeStampText = document.createTextNode(data.result.timestamp);
        idCell.appendChild(idText);
        logCell.appendChild(logText);
        timeStampCell.appendChild(timeStampText);
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

var getHistoricLog = function() {
  $.ajax({
    type: 'POST',
    url: 'getHistoricLog.php',
    dataType: "json",
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      /*console.log("Historic Log");
      console.log(id);
      console.log(data);*/

      if (data.status == 'ok'){

        if (table != null){
          for (var i = 0; i < data.result.length-1; i++) {
            table.deleteRow(-1);
         }
        }

        for (var i = 0; i < data.result.length-1; i++) {
          var newRow = table.insertRow(-1);
          var idCell = newRow.insertCell(0);
          var logCell = newRow.insertCell(1);
          var timeStampCell = newRow.insertCell(2);
          var idText = document.createTextNode(data.result[i].id);
          var logText = document.createTextNode(data.result[i].string);
          var timeStampText = document.createTextNode(data.result[i].timestamp);
          idCell.appendChild(idText);
          logCell.appendChild(logText);
          timeStampCell.appendChild(timeStampText);
        }
        id = data.result[data.result.length-2].id;
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

getHistoricLog();
setInterval(getHistoricLog,time);
