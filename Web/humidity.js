var time = 3000;
var idHumidity = 0;
var humidity = [];
var ctx = document.getElementById("humidityChart");
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [], // inserisco i timestamp
    datasets:[{
        data: humidity,
        label: "Humidity",
        borderColor: "#8e5ea2",
        fill: false
      }
    ]
  },
  options: {
     title: {
       display: true,
       text: "Storico umidità",
    }
  }
});


var getHistoricHumidity = function() {
  $.ajax({
    type: 'POST',
    url: 'getHistoricHumidity.php',
    dataType: "json",
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point
      // add new label and data point to chart's underlying data structures
      console.log(data.result);
      if (data.status == 'ok'){
        for (var i = 0; i < data.result.length; i++) {
          if (!myChart.data.labels.includes(data.result[i].timestamp)){
            myChart.data.labels.push(data.result[i].timestamp);
            myChart.data.datasets[0].data.push(data.result[i].value);
          }else{
            var index = myChart.data.labels.indexOf(data.result[i].timestamp);
            myChart.data.datasets[0].data[index] = data.result[i].value;
          }
        }
        idHumidity = data.result[data.result.length-1].id;

          // re-render the chart

        myChart.update();
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

var getHumidity = function() {
  $.ajax({
    type: 'POST',
    url: 'getHumidity.php',
    dataType: "json",
    data : {id:idHumidity},
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point
      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
        if (!myChart.data.labels.includes(data.result.timestamp)){
          myChart.data.labels.push(data.result.timestamp);
          myChart.data.datasets[0].data.push(data.result.value);
        }else{
          var index = myChart.data.labels.indexOf(data.result.timestamp);
          myChart.data.datasets[0].data[index] = data.result.value;
        }
        idHumidity=data.result.id;

          // re-render the chart

        myChart.update();
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

getHistoricHumidity();
setInterval(getHumidity,time);
