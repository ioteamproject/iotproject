var pression = [];
var time = 3000;
var idPression=0;
var ctx2 = document.getElementById("pressionChart");
var myChart = new Chart(ctx2, {
  type: 'line',
  data: {
    labels: [], // inserisco i timestamp
    datasets:[{
        data: pression,
        label: "Pression",
        borderColor: "#3e95cd",
        fill: false
      }]
  },
  options: {
     title: {
       display: true,
       text: "Storico pressione",
    }
  }
});


var getHistoricPression = function() {
  $.ajax({
    type: 'POST',
    url: 'getHistoricPression.php',
    dataType: "json",
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
          for (var i = 0; i < data.result.length; i++) {
            if (!myChart.data.labels.includes(data.result[i].timestamp)){
              myChart.data.labels.push(data.result[i].timestamp);
              myChart.data.datasets[0].data.push(data.result[i].value);
            }else{
              var index = myChart.data.labels.indexOf(data.result[i].timestamp);
              myChart.data.datasets[0].data[index] = data.result[i].value;
            }
          }
          idPression = data.result[data.result.length-1].id;
          // re-render the chart
          myChart.update();
        }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

var getPression = function() {
  $.ajax({
    type: 'POST',
    url: 'getPression.php',
    dataType: "json",
    data : {id:idPression},
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
        if (!myChart.data.labels.includes(data.result.timestamp)){
          myChart.data.labels.push(data.result.timestamp);
          myChart.data.datasets[0].data.push(data.result.value);
        }else{
          var index = myChart.data.labels.indexOf(data.result.timestamp);
          myChart.data.datasets[0].data[index] = data.result.value;
        }
        idPression=data.result.id;

        // re-render the chart

        myChart.update();
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

getHistoricPression();
setInterval(getPression,time);
