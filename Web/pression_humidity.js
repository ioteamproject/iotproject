// Our labels along the x-axis
// For drawing the lines
// IMPOSSIBILE AGGIUNGERE TIMESTAMP DINAMICI PROVENIENTI DA 2 FONTI DIVERSE 
var pression = [];
var humidity = [];
var timestamp = [];
var idPression = 0;
var idHumidity = 0;
var time = 3000;
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [], // inserisco i timestamp
    datasets: [
      {
        data: pression,
        label: "Pression",
        borderColor: "#3e95cd",
        fill: false
      },
      {
        data: humidity,
        label: "Humidity",
        borderColor: "#8e5ea2",
        fill: false
      }
    ]
  },
  options: {
     title: {
       display: true,
       text: "Storico pressione ed umidità",
    }
  }
});


function countElements(arr, elem){
  var counts=0;

  for (var i = 0; i < arr.length; i++) {
    if (arr[i] == elem){
      counts++;
    }
  }
  return counts;
}


var getHistoricPression = function() {
  $.ajax({
    type: 'POST',
    url: 'getHistoricPression.php',
    dataType: "json",
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
          for (var i = 0; i < data.result.length; i++) {
            if (!myChart.data.labels.includes(data.result[i].timestamp)){
              myChart.data.labels.push(data.result[i].timestamp);
              myChart.data.datasets[0].data.push(data.result[i].value);
            }else{
              var index = myChart.data.labels.indexOf(data.result[i].timestamp);
              myChart.data.datasets[0].data[index] = data.result[i].value;
            }
          }
          idPression = data.result[data.result.length-1].id;
          // re-render the chart
          myChart.update();
        }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

var getHistoricHumidity = function() {
  $.ajax({
    type: 'POST',
    url: 'getHistoricHumidity.php',
    dataType: "json",
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
        for (var i = 0; i < data.result.length; i++) {
          if (!myChart.data.labels.includes(data.result[i].timestamp)){
            myChart.data.labels.push(data.result[i].timestamp);
            myChart.data.datasets[1].data.push(data.result[i].value);
          }else{
            var index = myChart.data.labels.indexOf(data.result[i].timestamp);
            myChart.data.datasets[1].data[index] = data.result[i].value;
          }
        }
        idHumidity = data.result[data.result.length-1].id;

          // re-render the chart

        myChart.update();
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

var getHumidity = function() {
  $.ajax({
    type: 'POST',
    url: 'getHumidity.php',
    dataType: "json",
    data : {id:idHumidity},
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point
      console.log("Value");
      console.log(data.result);
      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
        if (!myChart.data.labels.includes(data.result.timestamp)){
          console.log("Inserisco");
          myChart.data.labels.push(data.result.timestamp);
          myChart.data.datasets[1].data.push(data.result.value);
        }else{
          console.log("Aggiorno");
          var index = myChart.data.labels.indexOf(data.result.timestamp);
          myChart.data.datasets[1].data[index] = data.result.value;
        }
        idHumidity=data.result.id;

          // re-render the chart

        myChart.update();
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

var getPression = function() {
  $.ajax({
    type: 'POST',
    url: 'getPression.php',
    dataType: "json",
    data : {id:idPression},
    success: function(data) {
      // process your data to pull out what you plan to use to update the chart
      // e.g. new label and a new data point

      // add new label and data point to chart's underlying data structures
      if (data.status == 'ok'){
        if (!myChart.data.labels.includes(data.result.timestamp)){
          myChart.data.labels.push(data.result.timestamp);
          myChart.data.datasets[0].data.push(data.result.value);
        }else{
          var index = myChart.data.labels.indexOf(data.result.timestamp);
          myChart.data.datasets[0].data[index] = data.result.value;
        }
        idPression=data.result.id;

        // re-render the chart

        myChart.update();
      }
    },
    error: function (xhr, httpStatusMessage, customErrorMessage) {
      alert(customErrorMessage);
    }
  });
};

function historic() {
  getHistoricPression();
//  getHistoricHumidity();
}

function now(){
  getPression();
  getHumidity();
}

historic();
setInterval(now,time);
