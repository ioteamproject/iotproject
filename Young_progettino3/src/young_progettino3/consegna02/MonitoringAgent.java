package young_progettino3.consegna02;

import task.MsgEvent;
import task.Utils;
import young_progettino.common.BasicEventLoopController;
import young_progettino3.serial.modulo_lab_2_2.msg.jssc.SerialCommChannel;

public class MonitoringAgent extends Thread{ // Used to receive ArduinoMessage

	SerialCommChannel channel;
	BasicEventLoopController controller;
	
	public MonitoringAgent(SerialCommChannel channel, BasicEventLoopController controller) throws Exception {
		this.channel = channel; 
		this.controller = controller;
	}
	
	public void run(){
		while (true){
			try {
				String msg = channel.receiveMsg();
				if (msg.equals(Utils.REQUEST_ARDUINO)) {
					System.out.println("Monitoring Agent Request Arduino "+ Utils.REQUEST_ARDUINO);
					this.controller.notifyEvent(new MsgEvent(Utils.REQUEST_ARDUINO,this));
				}else {
					System.out.println("Monitoring Agent Messaggio da Arduino "+msg);
					this.controller.notifyEvent(new MsgEvent(msg,this));
				}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
