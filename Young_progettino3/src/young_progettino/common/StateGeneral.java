package young_progettino.common;

/**
 * 
 * Enumeration for the State of Machine (AUTO/MANUAL) 
 * 
 * @author Thomas
 *
 *
 */
public enum StateGeneral {

	AUTO,
	
	MANUAL;
	
}
