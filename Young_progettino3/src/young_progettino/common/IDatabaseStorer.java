package young_progettino.common;

import java.sql.SQLException;

public interface IDatabaseStorer {

	void storeDataHumidity(Double humidityValue) throws SQLException;
	
	void storeDataPression(Double pressionValue) throws SQLException;

	void storeStateMode(String mode) throws SQLException;
	
	void storeLogString(String string) throws SQLException;

}
