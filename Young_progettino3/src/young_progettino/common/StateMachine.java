package young_progettino.common;

/**
 * 
 *	Singleton Class for the Machine State.
 * 
 * @author Thomas
 *
 */
public class StateMachine {
	
	private static StateGeneral stateMachine;
	private static StateHumidity stateHumidity;
	
	private static class StateMachineSingleton {
		private static final StateMachine SINGLETON = new StateMachine();
	}
	
	private StateMachine() {
		stateMachine = StateGeneral.AUTO;
		stateHumidity = StateHumidity.STOP;
	};

	// Creo il SINGLETON alla prima chiamata
	public static StateMachine getStateMachine() {
		return StateMachineSingleton.SINGLETON;
	}

	public StateGeneral getStateGeneral() {
		return stateMachine;
	}
	
	public void changeStateGeneral() {
		if (stateMachine.equals(StateGeneral.AUTO)) {
			stateMachine = StateGeneral.MANUAL;
		}else {
			 stateMachine = StateGeneral.AUTO;
		}
	}
	
	public StateHumidity getStateHumidity() {
		return stateHumidity;
	}
	
	public void changeStateHumidity() {
		if (stateHumidity.equals(StateHumidity.STOP)) {
			stateHumidity = StateHumidity.GO;
		}else {
			stateHumidity = StateHumidity.STOP;
		}
	}
}
