package young_progettino.common;

import java.sql.SQLException;

public interface Handler {

	void handle(Event ev) throws SQLException;

}
