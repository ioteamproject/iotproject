package young_progettino.common;

public interface Observer {

	boolean notifyEvent(Event ev);
}
