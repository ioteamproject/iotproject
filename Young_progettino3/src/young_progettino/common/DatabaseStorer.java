package young_progettino.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;

import task.Utils;

public class DatabaseStorer implements IDatabaseStorer{ // Builder

	private Connection conn;
	private final String url;
	private final String db;
	private final String username;
	private final String password;
	
	public DatabaseStorer(String url,String db, String username, String password) { 
		
		this.url = url;
		this.db = db;
		this.username = username;
		this.password = password;
	}
	
	/* Nelle query non vi � modo di parametrizzarle di pi�, quindi sono piuttosto statiche */
	
	@Override 
	public void storeDataHumidity(Double humidityValue) throws SQLException {
		
		QueryRunner run = this.connect();

		int humidityLine = run.update(conn, "INSERT INTO humidity (value) VALUES (?)",
				humidityValue); 		// valori
		
		this.closeDB();
				
		if (humidityLine == 1) {
			System.out.println("Dato dell'umidita\' memorizzato "+humidityValue);
		}else {
			System.out.println("Errore nella memorizzazione dell'umidita\'");
		}
		
	}
	
	@Override
	public void storeDataPression(Double pressionValue) throws SQLException {

		QueryRunner run = this.connect();

		int pressionLine = run.update(conn, "INSERT INTO pression (value) VALUES (?)",
				pressionValue); 		// valori
		
		this.closeDB();
		
		if (pressionLine == 1) {
			System.out.println("Dato della pressione memorizzato "+pressionValue);
		}else {
			System.out.println("Errore nella memorizzazione della pressione");
		}
	}
	
	@Override
	public void storeStateMode(String mode) throws SQLException {
		
		QueryRunner run = this.connect();

		int modeLine = run.update(conn, "INSERT INTO state_mode (mode) VALUES (?)",
				mode); 		// valori
		
		this.closeDB();
		
		if (modeLine == 1) {
			System.out.println("Dato dello stato memorizzato "+mode);
		}else {
			System.out.println("Errore nella memorizzazione dello stato");
		}
	}
	
	@Override
	public void storeLogString(String string) throws SQLException {

		QueryRunner run = this.connect();

		int logLine = run.update(conn, "INSERT INTO log (string) VALUES (?)",
				string); 		// valori
		
		this.closeDB();
		if (logLine == 1) {
			System.out.println("Dato del log memorizzato "+string);
		}else {
			System.out.println("Errore nella memorizzazione del log");
		}
	}
	
	private QueryRunner connect() throws SQLException {

		DbUtils.loadDriver(Utils.JDBC_DRIVER);

		conn = DriverManager.getConnection(url+db+Utils.FINAL_URL, username, password);
		
		return new QueryRunner();
	}
	
	private void closeDB() throws SQLException {

		DbUtils.close(conn);

	}

}
