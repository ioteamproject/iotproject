package young_progettino.common;


/**
 * 
 *  Enumeration for the Arduino pump (GO/STOP).
 * 
 * @author Thomas
 *
 */
public enum StateHumidity {

	GO,
	
	STOP;
	
}
