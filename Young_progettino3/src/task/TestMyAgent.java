package task;


import io.vertx.core.Vertx;

/**
 * 
 * Main della funzione
 *   
 * @author Thomas
 *
 */
public class TestMyAgent {
	
	public static void main(String[] args){

		MyAgent agent = new MyAgent(Utils.ARDUINO_PORT);
		agent.start();
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(Utils.PORT, agent);
		vertx.deployVerticle(service);
	}

}
