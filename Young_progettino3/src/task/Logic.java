package task;

import java.sql.SQLException;

import young_progettino.common.DatabaseStorer;
import young_progettino.common.Event;
import young_progettino.common.Handler;
//import young_progettino.common.IDatabaseStorer;
import young_progettino.common.ObservableTimer;
import young_progettino.common.StateGeneral;
import young_progettino.common.StateHumidity;
import young_progettino.common.StateMachine;
import young_progettino.common.Tick;
import young_progettino3.consegna02.MonitoringAgent;
import young_progettino3.serial.modulo_lab_2_2.msg.jssc.SerialCommChannel;

/**
 * 
 * Logics of the MSF.
 * 
 * @author Thomas
 *
 */
public class Logic implements Handler {

	private static final StateMachine state = StateMachine.getStateMachine();
	private final ObservableTimer timer;
	private final SerialCommChannel portArduino; 
	//private final IDatabaseStorer dbStorer;
	private Double humidityPerc;
	

	public Logic(ObservableTimer timer, SerialCommChannel portArduino){ // Arduino serve solo per i messaggi in uscita, ESP non c'� nella classe perch� non devo inviargli alcun messaggio

		this.timer = timer;
		this.humidityPerc = (double) 0;
		this.portArduino = portArduino;
		//this.dbStorer = new DatabaseStorer(Utils.DB_URL, Utils.DB, Utils.USERNAME, Utils.PASSWORD);
		
	}
	
	@Override
	public void handle(Event ev) throws SQLException {

		if (ev instanceof MsgEvent) { // Se � un messaggio, ed � un messaggio
			String msg = ((MsgEvent) ev).getMsg();
			System.out.println("Received: " + msg);

			/*  ARDUINO mi notifica il cambiamento di stato */
			if (((MsgEvent) ev).getClasse() instanceof MonitoringAgent) {
				
				if (msg.equals(Utils.REQUEST_ARDUINO)) {
					
					/* Notifico nei log il fatto che ho cambiato stato */
					this.storeLog(Utils.SWITCH_MODE);
					
					/* Cambio lo stato */
					this.changeGeneralState();

					
					/* Notifico il camiamento nel DB 
					try {
						this.dbStorer.storeStateMode(state.getStateGeneral().toString());
					} catch (SQLException e) {
						e.printStackTrace();
					}*/
					
				}else {
					this.storeLog(Utils.ARDUINO_MESSAGE);
					this.storePression(msg);
				}
				
			}else {
				this.storeLog(Utils.ESP_MESSAGE);
				this.controllerLogics(msg);
			}

		} else if (ev instanceof Tick) {
			System.out.println("Evento del Timer, stoppo l'erogazione");
			this.storeLog(Utils.TIMER_EVENT);
			this.changeState(StateHumidity.STOP);
			this.portArduino.sendMsg(String.valueOf(Utils.P_STOP));
		}

	}	


	/* Se il messaggio proviene da Arduino memorizzo l'umidit� altrimenti proseguo e notifico l'Arduino */
	private void controllerLogics(String msg) throws SQLException {
		this.humidityPerc = Double.parseDouble(msg);
		this.storeHumidity();
		this.storeLog(Utils.SENDED_MESSAGE);
		String pression;
 		
		if (state.getStateGeneral().equals(StateGeneral.AUTO)) {  // Accendo le pompe:
			
			if ((humidityPerc < Utils.U_MAX) || // . se l'umidit� � troppo bassa (P.S. Da noi U_MIN corrisponde a U_MAX secondo le direttive del prof)
					((state.getStateHumidity() == StateHumidity.GO)
							&& (humidityPerc <= Utils.U_MAX + Utils.DELTA_U))) { // . se non siamo andati oltre la soglia

				this.changeState(StateHumidity.GO);
				/* Minima umidit� percepita massima pressione da applicare nelle pompe e viceversa */
				if (humidityPerc < Utils.U_MIN) {

					pression = Utils.P_MAX;
				
				}else if (humidityPerc < Utils.U_MED) {
					pression = Utils.P_MED;
				
				}else {
					pression = Utils.P_MIN;
				}
				this.storePression(pression);
				this.portArduino.sendMsg(pression);
			} else {
				changeState(StateHumidity.STOP);
				pression = Utils.P_STOP;
				this.storePression(pression);
				this.portArduino.sendMsg(String.valueOf(Utils.P_STOP));
			}
		}else {
			/* Se sono in MANUAL invio ad Arduino l'umidit� */
			this.portArduino.sendMsg(this.humidityPerc.toString());
		}
	}

	private void storePression(String pression) throws SQLException {
		
		double pressionValue;
		switch(pression) {
		case Utils.P_MAX:  
			pressionValue=Utils.P_MAX_VALUE;
			break;
		case Utils.P_MED:  
			pressionValue=Utils.P_MED_VALUE;
			break;
		case Utils.P_MIN:  
			pressionValue=Utils.P_MIN_VALUE;
			break;
		case Utils.P_STOP:  
			pressionValue=Utils.P_STOP_VALUE;
			break;
		default:
			pressionValue = Double.valueOf(pression);
		}
		System.out.println("Logic StorePression "+pressionValue);
		//this.dbStorer.storeDataPression(pressionValue);
	}
	
	private void storeHumidity() throws SQLException {
		
		System.out.println("Logic StoreHumidity "+this.humidityPerc);
		//dbStorer.storeDataHumidity(this.humidityPerc);
	}
	
	private void storeLog(String msg) throws SQLException {
		
		System.out.println("Logic StoreLog "+msg);
		//dbStorer.storeLogString(msg);
	}
	
	/* Cambia solamente il Timer se v� adoperato e setta correttamente gli stati */
	private synchronized void changeState(StateHumidity newState) { 

		if (newState != state.getStateHumidity()) {

			state.changeStateHumidity();
			
			if (state.getStateHumidity().equals(StateHumidity.GO)) {
				System.out.println("Le pompe hanno iniziato ad andare");
				timer.scheduleTick(Utils.T_MAX);
				System.out.println("Inizio il Timer");
			} else {
				/* Stoppo il pompaggio ad Arduino */
				System.out.println("Stoppo le pompe");
				timer.stop();
				System.out.println("Stoppo il Timer");
			}
		}
	}
	
	/* Se chiamato a causa dell'evento di Arduino */
	private synchronized void changeGeneralState() {
		
		timer.stop(); // E' pi� rapido che un controllo dello stato se � Auto o no, cos� eseguo direttamente lo stop
		state.changeStateGeneral();
		/* Cambio lo stato nel DB 
		try {
		this.dbStorer.storeStateMode(state.getStateGeneral().toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
	}

}
