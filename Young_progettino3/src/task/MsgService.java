package task;

import young_progettino.serial.modulo_lab_4_2.ExtendedSerialCommChannel;
import young_progettino3.serial.modulo_lab_2_2.msg.CommChannel;


public class MsgService extends young_progettino.common.Observable {

	private CommChannel channel; // Qui andranno inserite bt o wifi
	private String port;
	private int rate;
	
	public MsgService(String port, int rate){
		this.port = port;
		this.rate = rate;
	}
	
	void init(){
		try {
			channel = new ExtendedSerialCommChannel(port, rate);	
			// channel = new SerialCommChannel(port, rate);	
			System.out.println("Waiting Arduino for rebooting...");		
			Thread.sleep(4000);
			System.out.println("Ready.");		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		new Thread(() -> {
			while (true) {
				try {
					String msg = channel.receiveMsg();
					System.out.println("received "+msg);
					this.notifyEvent(new MsgEvent(msg,this));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}).start();
	}
	
	public void sendMsg(String msg) {
		channel.sendMsg(msg);
		System.out.println("sent "+msg);
	}
	
}
