package task;

import young_progettino.common.*;

public class MsgEvent implements Event {
	
	private String msg;
	private final Object classe;
	
	public MsgEvent(String msg, Object classe) {
		this.msg = msg;
		this.classe = classe;
	}

	public String getMsg(){
		return msg;
	}
	
	public Object getClasse(){
		return this.classe;
	}
	
	public void setMSg(String msg) {
		this.msg = msg;
	}
}
