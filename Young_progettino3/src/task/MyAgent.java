package task;


import young_progettino.common.*;
import young_progettino3.consegna02.MonitoringAgent;
import young_progettino3.serial.modulo_lab_2_2.msg.jssc.SerialCommChannel;

/**
 * @author Thomas
 *
 *	Controller for the event (Arduino and ESP)
 *
 */
public class MyAgent extends BasicEventLoopController {  
	
	private ObservableTimer timer; 
	private SerialCommChannel portArduino;
	private Handler logic;

	private MonitoringAgent agentArduino; // is used in MonitoringAgent class (reflection)
	
	/* Ricevo ad Eventi, invio sui canali */
	public MyAgent(String portArduino){   
		super();

		this.initSerial(portArduino);  // Qua gestisco i messaggi di Arduino

		this.timer = new ObservableTimer();
		this.timer.addObserver(this);
		this.logic = new Logic(this.timer, this.portArduino);
	}


	private void initSerial(String portArduino) {
		
		try {
			this.portArduino = new SerialCommChannel(portArduino, Utils.ARDUINO_RATE);
			this.agentArduino = new MonitoringAgent(this.portArduino, this);
			this.agentArduino.start(); // Start Port Thread
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	@Override
	protected void processEvent(Event ev) {

		try {
			this.logic.handle(ev);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
