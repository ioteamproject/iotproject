package task;

import java.util.Date;
import java.util.LinkedList;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import young_progettino.common.BasicEventLoopController;

/*
 * Data Service as a vertx event-loop 
 */
public class DataService extends AbstractVerticle {

	private final int port;
	private static final int MAX_SIZE = 10;
	private LinkedList<DataPoint> values;
	private BasicEventLoopController controller;
	private final String api;
	
	public DataService(int port, BasicEventLoopController controller) {
		values = new LinkedList<>();		
		this.port = port;
		this.controller = controller;
		this.api = Utils.API;
	}

	@Override
	public void start() {		
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.post(this.api).handler(this::handleAddNewData);
		router.get(this.api).handler(this::handleGetData);		
		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(port);

		log("Service ready.");
	}
	
	private void handleAddNewData(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		// log("new msg "+routingContext.getBodyAsString());
		JsonObject res = routingContext.getBodyAsJson();
		if (res == null) {
			sendError(400, response);
		} else {
			String place = res.getString(Utils.ESP_PLACE_KEY);
			float value = res.getFloat(Utils.ESP_DATA_KEY);
			long time = System.currentTimeMillis();
			
			values.addFirst(new DataPoint(value, time, place));
			if (values.size() > MAX_SIZE) {
				values.removeLast();
			}
			log("New value: " + value + " from " + place + " on " + new Date(time));
			this.controller.notifyEvent(new MsgEvent(String.valueOf(value),this));
			
			
			response.setStatusCode(200).end();
		}
	}
	
	private void handleGetData(RoutingContext routingContext) {
		JsonArray arr = new JsonArray();
		for (DataPoint p: values) {
			JsonObject data = new JsonObject();
			data.put("time", p.getTime());
			data.put("humidity", p.getValue()); 
			data.put("place", p.getPlace());
			arr.add(data);
		}
		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(arr.encodePrettily());
	}
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}
}