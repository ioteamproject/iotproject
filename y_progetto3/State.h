#include "config.h"
#include "Arduino.h"

  class State{
  
    private:
    
        /* Here will be the instance stored. */
    
        static State* instance;
        Status statum;
        int eroga;
        int distanza;    //valore in cm distanza
        int portata;     //valore tra 0 e 255 per rappresentare l'intensità del LED2
        int umidita;
        int conn;
        
        /* Private constructor to prevent instancing. */
        State();
        ~State();

    public:

        Status getValue();
        
        void setValue(Status v);

        void setErogazione(int val);

        int getErogazione();

        static State* getInstance();

        void setDistanza(int val);

        int getDistanza();

        void setPortata(int val);

        int getPortata();

        void setUmidita(int val);

        int getUmidita();

        void setConn(int conn);

        int getConn();


};

