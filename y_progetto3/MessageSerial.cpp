#include "MessageSerial.h"
#include "MsgService.h"
#include "Arduino.h" 
#include "config.h"

MessageSerial::MessageSerial(){

}
  
void MessageSerial::init(int period){
  Task::init(period); 
}
  
void MessageSerial::tick(){
  if(State::getInstance()->getValue() == AUTO){
    //MsgService.sendMsg("AUTO");
    if (MsgService.isMsgAvailable()) {
    Msg* msg = MsgService.receiveMsg();
    if (msg->getContent() == "Pmin"){
       State::getInstance()->setErogazione(1);
       State::getInstance()->setPortata(2);
       delay(500);
    } else if(msg->getContent() == "Pmed"){
       State::getInstance()->setErogazione(1);
       State::getInstance()->setPortata(150);
       delay(500);
    } else if(msg->getContent() == "Pmax"){
       State::getInstance()->setErogazione(1);
       State::getInstance()->setPortata(255);
       delay(500);
    } else if(msg->getContent() == "Stop"){
       State::getInstance()->setErogazione(0);
       State::getInstance()->setPortata(0);
       delay(500);
    }
    delete msg; 
    }
  } else {          //modalità MANUAL
    //MsgService.sendMsg("MANUAL");
    if(State::getInstance()->getPortata() != 0){
      MsgService.sendMsg("A"+State::getInstance()->getPortata());
    }
    
    if (MsgService.isMsgAvailable()) {
    Msg* msg = MsgService.receiveMsg();
    int umidita = (int)msg;
    State::getInstance()->setUmidita(umidita);         //settare umidità inviata da Java
    
    delete msg;
    }
  }
}
