/*
 *  This header file stores symbols that concerns 
 *  the configuration of the system.
 *
 */
#ifndef __CONFIG__
#define __CONFIG__

#include "enumstate.h"

// LEDS
#define N_LED    3 
#define LED1_PIN 9
#define LED2_PIN 6 // fade
#define LED3_PIN 5

//BLUETOOTH
#define RX_PIN 3
#define TX_PIN 4


// DIST
#define TRIG_PIN 8
#define ECHO_PIN 7

//SERVO MOTORE
#define M_PIN 2

 
//        const

// distance
#define DISTMIN 30  // Distance min=0.30m for Bluetooth connection
// time
#define TMAX 5000 // Max time to erogate water

#endif
