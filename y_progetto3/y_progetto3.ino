#include "Scheduler.h"
#include "Servotask.h"
#include "BlinkTask.h"
#include "Dist_task.h"
#include "MessageSerial.h"
#include "SoftwareSerial.h"
#include "MsgServiceBT.h"
#include "MessageBT.h"

Scheduler sched;
MsgServiceBT* msgServiceBT= new MsgServiceBT(3,4);

void setup(){

  sched.init(50);
  MsgService.init();
  msgServiceBT->init();
  
  Serial.begin(9600);
  State::getInstance()->setValue(AUTO);
  State::getInstance()->setErogazione(0);
  State::getInstance()->setDistanza(31);
  State::getInstance()->setPortata(0);
  State::getInstance()->setUmidita(0);
  State::getInstance()->setConn(0);

  Dist* dist = new Dist(TRIG_PIN, ECHO_PIN);

  Task* t0 = new Servotask(2);
  t0->init(500);
  sched.addTask(t0);

  Task* t1 = new BlinkTask(LED1_PIN, LED2_PIN, LED3_PIN, dist);
  t1->init(500);
  sched.addTask(t1);

  Task* t2 = new MessageSerial();
  t2->init(500);
  sched.addTask(t2);

  Task* t3 = new MessageBT(msgServiceBT);
  t3->init(500);
  sched.addTask(t3);

}


void loop(){
    sched.schedule();
}
