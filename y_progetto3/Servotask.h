#ifndef __SERVOTASK__
#define __SERVOTASK__

#include "Task.h"
#include "servo_motor_impl.h"
//using namespace std;

//extern State state;

class Servotask: public Task {

  int pin;
  int pos;   
  int delta;
  ServoMotor* pMotor;

public:

  Servotask(int pin);  
  void init(int period);
  void tick();
};

#endif
