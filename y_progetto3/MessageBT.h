#ifndef __MESSAGEBT__
#define __MESSAGEBT__

#include "MsgServiceBT.h"
#include "Task.h"

class MessageBT: public Task {

  MsgServiceBT* msgServiceBT;

public:

  
  MessageBT(MsgServiceBT* msgServiceBT);
  void init(int period);  
  void tick();
};

#endif
