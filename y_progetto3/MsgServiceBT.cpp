#include "Arduino.h"
#include "MsgServiceBT.h"


MsgServiceBT::MsgServiceBT(int rxPin, int txPin){
  channel = new SoftwareSerial(rxPin, txPin);
}

void MsgServiceBT::init(){
  content.reserve(256);
  channel->begin(9600);
}

bool MsgServiceBT::sendMsg(MsgBT msg){
  channel->println(msg.getContent());  
}

bool MsgServiceBT::isMsgAvailable(){
  return channel->available();
}

MsgBT* MsgServiceBT::receiveMsg(){
  while (channel->available()) {
    char ch = (char) channel->read();
    if (ch == '\n'){
      MsgBT* msg = new MsgBT(content);  
      content = "";
      return msg;    
    } else {
      content += ch;      
    }
  }
  return NULL;  
}
