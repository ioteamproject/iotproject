#include "Dist_task.h"

Dist_task::Dist_task(Dist* dist){
  
  this->dist=dist;
}

void Dist_task::init(int period){
  Task::init(period);
}


void Dist_task::tick(int basePeriod){
  noInterrupts();
    //Serial.println(dist->getDistance());
		if (dist->getDistance() <= 30){
      //Serial.println("Distanza minore a 30cm");
			State::getInstance()->setValue(MANUAL);
		}else{											 // reset if distance errated
      State::getInstance()->setValue(AUTO);
			this->reset();
		}
	interrupts();
 }                                                                                            
