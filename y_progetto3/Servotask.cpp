#include "Arduino.h"
#include "Servotask.h"

Servotask::Servotask(int pin){
  this->pin = pin;
}

void Servotask::init(int period){
  Serial.begin(9600);
  Task::init(period);
  pMotor = new ServoMotorImpl(pin);
  pos = 0;
  delta = 1;
}

void Servotask::tick(){
  Serial.begin(9600);
  if(State::getInstance()->getErogazione() == 1){
    pMotor->on();
    for (int i = 0; i < 180; i++) {
    pMotor->setPosition(pos);
    delay(10);            
    pos += delta;
  }
  pMotor->off();
  pos -= delta;
  delta = -delta;
  delay(1000);
  }else {
    //Serial.begin(9600);
    //Serial.println("servo spento");
  }
  
}
