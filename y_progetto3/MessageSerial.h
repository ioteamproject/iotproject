#ifndef __MESSAGESERIAL__
#define __MESSAGESERIAL__

#include "MsgService.h"
#include "Task.h"

class MessageSerial: public Task {


public:

  MessageSerial();
  void init(int period);  
  void tick();
};

#endif
