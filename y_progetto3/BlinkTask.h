#ifndef __BLINKTASK__
#define __BLINKTASK__

#include "Task.h"
#include "Led.h"
#include "Distantiometer.h"
#include "MsgService.h" //messaggi java

class BlinkTask: public Task {

  int pin[2];
  int pin3;
  Light* led[2];
  enum { ON, OFF} state;
  Dist* dist;

public:

  BlinkTask(int pin0, int pin1, int pin2, Dist* dist);  
  void init(int period);  
  void tick();
};

#endif
