#ifndef __DIST_TASK__
#define __DIST_TASK__

#include "Task.h"
#include "Distantiometer.h"

class Dist_task: public Task { 

  Dist* dist;

public:

  Dist_task(Dist* dist);
  void init(int period);
  void tick(int basePeriod);
};

#endif
