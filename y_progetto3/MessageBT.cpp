﻿#include "MessageBT.h"
#include "MsgServiceBT.h"
#include "Arduino.h" 
#include "config.h"

MessageBT::MessageBT(MsgServiceBT* msgServiceBT){
  this->msgServiceBT = msgServiceBT;
}

void MessageBT::init(int period){
  Task::init(period);
}

void MessageBT::tick(){
  if (msgServiceBT->isMsgAvailable() && State::getInstance()->getConn() == 0) {
    MsgBT* msg = msgServiceBT->receiveMsg();
    //Serial.println(msg->getContent());
    if (msg->getContent() == "bluetooth"){        //Se L'app android si collega, mi invia un messaggio, setto la comunicazione a true
      State::getInstance()->setConn(1);
      msgServiceBT->sendMsg(MsgBT("MANUAL\n"));           //Se sono in modalità manuale aspetto i messaggi
      Serial.println(State::getInstance()->getConn());
      State::getInstance()->setPortata(125);      //al momento della connessione setto la portata = 100, cosi l'app android mi invia semplicemente se aumentare o diminuire
    }
    delete msg;
  } 
  if (State::getInstance()->getValue() == MANUAL && State::getInstance()->getConn() == 1){
    //msgServiceBT->sendMsg(MsgBT("MANUAL\n"));           //Già inviato
    int umidita = State::getInstance()->getUmidita();
    msgServiceBT->sendMsg(MsgBT("" +umidita+'\n'));
    delay(500);
    
    if(msgServiceBT->isMsgAvailable()){
      MsgBT* msg = msgServiceBT->receiveMsg();
      if (msg->getContent() == "EXIT"){        //Se L'app android si collega, mi invia un messaggio, setto la comunicazione a true
        State::getInstance()->setConn(0);
        Serial.println(State::getInstance()->getConn());
        State::getInstance()->setPortata(125);      //al momento della connessione setto la portata = 100, cosi l'app android mi invia semplicemente se aumentare o diminuire
      }else if (msg->getContent() == "START"){
        State::getInstance()->setErogazione(1);
        delay(500);
      } else if (msg->getContent() == "PIU"){
          int portata = State::getInstance()->getPortata();
          if(portata < 250){
            State::getInstance()->setPortata(portata);
          } else {
            portata+=5;                                   //con i tasti piu e meno su android, aumento o diminuisco la portata di 5 alla volta
            State::getInstance()->setPortata(portata);
          }
          delay(500);
      } else if(msg->getContent() == "MENO"){
          int portata = State::getInstance()->getPortata();
          if(portata < 5){
            State::getInstance()->setPortata(portata);
          } else {
            portata-=5;
            State::getInstance()->setPortata(portata);
          }
          delay(500);
      } else if(msg->getContent() == "STOP"){
          State::getInstance()->setErogazione(0);
          delay(500);
      }
      delete msg;
    }
  } //(else)  se è AUTO non ricevo e non invio messaggi
}
