#include "BlinkTask.h"
#include "MsgService.h" //messaggi java

BlinkTask::BlinkTask(int pin0, int pin1, int pin2, Dist* dist){
  this->pin[0] = pin0;
  this->pin[1] = pin2;
  this->pin3 = pin1;
}
  
void BlinkTask::init(int period){
  pinMode(pin3, OUTPUT);
  Task::init(period);
  for (int i = 0; i < 2; i++){
    led[i] = new Led(pin[i]); 
  }    
}
  
void BlinkTask::tick(){
  if(State::getInstance()->getValue() == AUTO){
    led[0]->switchOn();
    delay(15);
  } else if(State::getInstance()->getValue() == MANUAL){
    led[1]->switchOn();
    delay(15);
  }
  if(dist->getDistance() <= 30 && State::getInstance()->getErogazione() == 0 && State::getInstance()->getConn() == 1){
    if(State::getInstance()->getValue() == AUTO){
        State::getInstance()->setValue(MANUAL);
        MsgService.sendMsg("C"); //invio cambio di stato in MANUAL
        led[0]->switchOff();
        led[1]->switchOn();
        delay(30);
    }
    //Serial.begin(9600);
    //Serial.println(State::getInstance()->getValue());
    // }else if (dist->getDistance() > 30 || State::getInstance()->getConn() == 0){ // DA TESTARE
  } else {
    if(State::getInstance()->getValue() == MANUAL){
        State::getInstance()->setValue(AUTO);
        MsgService.sendMsg("C"); //invio cambio di stato in AUTO
        led[1]->switchOff();
        led[0]->switchOn();
        delay(30);
    }
    //Serial.begin(9600);
    //Serial.println(State::getInstance()->getValue());
  }

  if(State::getInstance()->getErogazione() == 1){
    analogWrite(pin3, State::getInstance()->getPortata());
    delay(15);
  } else {
    //spegni analogWrite
    analogWrite(pin3, 0);
    delay(15);
  }
  
  
}
