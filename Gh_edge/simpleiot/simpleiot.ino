#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

/* wifi network name */
char* ssidName = "HUAWEI P20";
/* WPA2 PSK password */
char* pwd = "79679d813f55";
/* service IP address */ 
char* address = "http://169a6939.ngrok.io";

void setup() { 
  Serial.begin(115200);                                
  WiFi.begin(ssidName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
  Serial.println("Connected: \n local IP: "+WiFi.localIP());
}

int sendData(String address, float value){  
   HTTPClient http;    
   http.begin(address + "/api/data");      
   http.addHeader("Content-Type", "application/json");     
   String json = "{\"humidity\" : "+ String(value) +" }";
   int retCode = http.POST(json);   
   http.end();        
   return retCode;
}
   
void loop() { 
 if (WiFi.status()== WL_CONNECTED){   

    
   /* read sensor */
   float readValue = (float) analogRead(A0);
   float value = (float)map(readValue,0,1023,0,100);
   
   /* send data */
      
   int code = sendData(address, value);
  Serial.println(code);
   /* log result 
   if (code == 200){
     Serial.println("ok");   
   } else {
     Serial.println("error");
   }*/
 } else { 
   Serial.println("Error in WiFi connection");   
 }
 
 delay(1000);  
 
}
