package smartgreenhouse.com.sgh;

public class C {
    public static final String LIB_TAG = "BluetoothLib";

    public static final String APP_LOG_TAG = "BT CLN";

    public class bluetooth {
        public static final int ENABLE_BT_REQUEST = 1;
        public static final String BT_DEVICE_ACTING_AS_SERVER_NAME = "DSD TECH HC-05";
        public static final String BT_SERVER_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    }
    public class channel {
        public static final int MESSSAGE_RECEIVED = 0;
        public static final int MESSAGE_SENT = 1;
    }

    public static final char MESSAGE_TERMINATOR = '\n';
    public static final String CONNESSO = "bluetooth";
    public static final String START = "START";
    public static final String STOP = "STOP";
    public static final String PIU = "PIU";
    public static final String MENO = "MENO";
    public static final String EXIT = "EXIT";
}
