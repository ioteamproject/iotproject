package smartgreenhouse.com.sgh;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    private BluetoothChannel btChannel;
    private int litriErogati=125;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initBT();

        initUI();
    }

    private void initBT(){
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }
    }

    private void initUI() {
        findViewById(R.id.connectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    connectToBTServer();
                } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                    bluetoothDeviceNotFound.printStackTrace();
                }
            }
        });

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btChannel.sendMessage(C.START);
            }
        });
        findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btChannel.sendMessage(C.STOP);
            }
        });
        findViewById(R.id.piu).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(litriErogati<255) litriErogati+=5;
                else litriErogati=0;
                TextView text=findViewById(R.id.WaterValue);
                text.setText(String.format(String.valueOf(litriErogati)+"l/m"));
                btChannel.sendMessage(C.PIU);
            }
        });
        findViewById(R.id.meno).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(litriErogati>0) litriErogati-=5;
                else litriErogati=0;
                TextView text=findViewById(R.id.WaterValue);
                text.setText(String.format(String.valueOf(litriErogati)+"l/m"));
                btChannel.sendMessage(C.MENO);
            }
        });
        findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btChannel.sendMessage(C.EXIT);
                onDestroy();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        btChannel.close();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initBT();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK){
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED){
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);
        final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        AsyncTask<Void, Void, Integer> execute = new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : connected to server on device %s",
                        serverDevice.getName()));

                findViewById(R.id.connectBtn).setEnabled(false);

                btChannel = channel;
                btChannel.sendMessage(C.CONNESSO);

                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                   @Override
                    public void onMessageReceived(String receivedMessage) {
                       if (receivedMessage.equals("MANUAL"+C.MESSAGE_TERMINATOR)) {
                           ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : Manual mode"));
                       } else {
                           TextView text = findViewById(R.id.HumValue);
                           text.setText(receivedMessage);
                       }
                    }

                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : unable to connect, device %s not found!",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        btChannel.close();
    }
}

