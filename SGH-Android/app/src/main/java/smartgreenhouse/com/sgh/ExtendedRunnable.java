package smartgreenhouse.com.sgh;

public interface ExtendedRunnable extends Runnable {
    void write(byte[] bytes);
    void cancel();
}
